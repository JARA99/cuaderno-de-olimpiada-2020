#   Cuaderno de Olimpiadas 2020

En esta repo, encontrarán las notas de clase, y las presentaciones que se utilicen durante el curso de preparación para la **Olimpiada Nacional de Ciencias de Guatemala** del 2020, para la categoría de **Física diversificado**

*   Libro de texto: [Sears y Zemansky, Tomo 1](Recursos/Libros/Zemansky1.pdf)
*   Libro de texto: [Sears y Zemansky, Tomo 2](Recursos/Libros/Zemansky2.pdf)
*   Libro de texto: [Tippens](Recursos/Libros/TIPPENS.pdf)
*   Libro de texto: [Resnik](Recursos/Libros/Resnik.pdf)
*   Libro de texto: [Sears y Zemansky ed 13, Tomo 1](Recursos/Libros/Zemansky1-13.pdf)
*   Programa del curso: [PDF](Recursos/Otros/Programa.pdf)
*   Contenido de olimpiadas: [PDF](Recursos/Otros/Fisica-Diversificado-1.pdf)

##  Notas de clase

### Listado de cuadernos

1.  Cinemática: [PDF](Notas_de_clase_PDF/Cinematica.pdf), [MD](Notas_de_clase_MD/Cinematica.md)
2.  Dinámica: [PDF](Notas_de_clase_PDF/Dinamica.pdf), [MD](Notas_de_clase_MD/Dinamica.md)
3.  Trabajo y Energía: [PDF](Notas_de_clase_PDF/Trabajo%20y%20Energía.pdf), [MD](Notas_de_clase_MD/Trabajo%20y%20Energía.md)
4.  Conservación del momento: [PDF](Notas_de_clase_PDF/Conservación%20del%20momento.pdf), [MD](Notas_de_clase_MD/Conservación%20del%20momento.pdf)
5.  Electromagnetismo: [PDF](Notas_de_clase_PDF/Electromagnetismo.pdf), [MD](Notas_de_clase_MD/Electromagnetismo.md)
6.  Resolución de dudas: [PDF](Notas_de_clase_PDF/Resolución%20de%20Dudas%20de%20problemas.pdf), [MD](Notas_de_clase_MD/Resolución%20de%20Dudas%20de%20problemas.md)

### Explicación breve de las notas de clase

Estaremos trabajando las clases en un archivo llamado *Markdown*, el cual me permite escribir ecuaciones de una manera más eficiente durante la clase. Se realizará un archivo nuevo para cada tema nuevo, así todo estará ordenado. Si desean editar, o revisar estas notas en el archivo *Makrdown*, necesitan un programa para abrirlo, yo les recomiendo [Typora](https://typora.io/). De cualquier modo, estarán disponibles en formato [PDF](Notas_de_clase_PDF/)

##  Otros recursos

1.  Resumen de vectores: [PDF](Recursos/Repasos_y_resumenes/Resumen_de_vectores.pdf)
2.  Hoja de Trabajo 1: [PDF](Recursos/Hojas_de_trabajo/HojaDeTrabajo1.pdf)

##  Jupyter Notebooks

### Explicación breve de *Jupyter Notebook*

En algunas clases realizaremos alguna actividad aprovechando el uso de las computadoras, de modo que realizaremos algunas gráficas y simulaciones con la ayuda de python. Para esto utilizaremos una herramienta conocida como *Jupyter Notebook* (*.ipynb*), que permite mezclar código y texto en formato *Markdown*. Será útil para que puedan visualizar e incluso editar en sus propios navegadores.

### Como ver los archivos *.ipynb*

Para acceder a estos archivos, tienen tres opciones distintas, la primera es **acceder desde este mismo repositorio** (Básicamente un repositorio es una carpeta con archivos, usualmente de código, que se comparte de manera pública y se almacena en la nube en plataformas como GitHub o GitLab. Es ideal para mantener todos los archivos actualizados en la nube, y poder revisar versiones anteriores de los mismos). 

El único problema con abrir los archivos desde aquí, es que algunos símbolos matemáticos no se despliegan de la manera correcta.

La otra opción, y es la que les aconsejo, es **abrir los archivos en modo lectura** en *nbviewer*, para esto solo basta con ir a los enlaces descritos abajo. La única desventaja de este método es que no siempre estarán actualizados, pueden intentar actualizarlos agregando al final del link el siguiente código: 

    ?flush_cache=true

Pero me he encontrado con que esta estrategia no siempre es del todo efectiva.

Por último, pueden **abrir los archivos desde la plataforma de *Jupyter***, en la que no solo podrán leerlos, si no **editarlos**, nótese que para guardar los cambios deberán descargarlos una vez editados. Este método nos será útil en clases avanzadas, donde espero correr algunas simulaciones, y así tendrán el código disponible después para hacerle cambios a los datos y hacer sus propias simulaciones.

La principal desventaja que tiene este método es que a veces es un poco tardado para cargar, ya que no solo está leyendo el código, si no compilándolo.

<!--### *JupyterNotebooks* en modo lectura:

*   [Cinemática](https://nbviewer.ipython.org/urls/gitlab.com/JARA99/cuaderno-de-olimpiada-2020/-/raw/master/Cuadernos/Cinematica.ipynb)

### *JupyterNotebooks* en editor:

*   [Cinemática](https://mybinder.org/v2/gl/JARA99%2Fcuaderno-de-olimpiada-2020/a0ced8dc52cac7bf53f20cc2460b89abc41d01d2?filepath=Cuadernos%2FCinematica.ipynb)-->

## Otros enlaces:

*   [Presentación en PDF](Presentacion/Presentación.pdf)

*   [Repositorio en Binder](https://mybinder.org/v2/gl/JARA99%2Fcuaderno-de-olimpiada-2020/master)

*   [Repositorio en GitLab](https://gitlab.com/JARA99/cuaderno-de-olimpiada-2020.git)