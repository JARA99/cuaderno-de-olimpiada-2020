![](G1.png)
$$
x_1=v_{1}t+x_{0,1}
$$

$$
x_2=\cancelto{0}{x_{o,2}}+\cancelto{0}{v_{o,2}t}+\frac{1}{2}at^2
$$


$$
a=x_2/t^2
$$

$$
\frac{x_1-x_{0,1}}{v_1}=t
$$


$$
a=x_2*\frac{1}{(\frac{x_1-x_{0,1}}{v_1})^2}\\\\
a=\frac{x_2*v_1^2}{(x_1-x_{0,1})^2}
$$


Problema 5, alcances.
$$
y_1(t)=\cancelto{0}{y_o}+\cancelto{0}{v_ot}+\frac{1}{2}gt^2
$$

$$
y_2(t)=\cancelto{0}{y_o}+v_o(t-t_o)+\frac{1}{2}g(t-t_o)^2
$$

$$
(t-t_o)\\
\text{Al principio:}\\
(t_o-t_o)=0
$$

