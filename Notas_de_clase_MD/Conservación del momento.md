#	Conservación del momento

##	Definición del momento

Linear momenta, momentum, momento, momento lineal...


$$
\vec{p}=m\vec{v}
$$

$$
\Delta \vec{p}=0\\
\Longrightarrow\\
p_f=p_o
$$



##	Tipos de choque

* Choque elástico
* Choque inelástico
* Choque completamente inelástico

**En todos los tipos de choque, el momento se conserva.**

###	Choque elástico

* Se conserva la energía

* Se conserva el momento

  

  Tenemos dos ecuaciónes:
  $$
  mv_o=Mv_f\\
  \frac{1}{2}mv_o^2=\frac{1}{2}Mv_f^2
  $$
  

  ###		Choque inelástico

  

* No se conserva la energía

* Se conserva el momento

Solo hay una ecuación
$$
mv_o=Mv_f
$$


###	Choque completamente inelástico

* No se conserva la energía
* Se conserva el momento
* Después del choque, los objetos quedan pegados

$$
M_f=\sum_i m_i~~~\text{con $m_i$ las masas que chocaron}
$$

$$
m_ov_o=M_f v_f
$$





##	Ejemplo

> Considere dos partículas, una que se mueve a velocidad $v$ y la otra en reposo, si ambas partículas tienen la misma masa, ¿cuál será la velociadad de la partícula originalmente en reposo, después de un choque elástico?



#	$I$

$$
\vec{p}=mv\\
K=\frac{1}{2}mv^2
$$



##	$II$

$$
\vec{p}=mv_{f1}+mv_{f2}=m(v_{f1}+v_{f2})\\
K=\frac{1}{2}mv_{f1}^2+\frac{1}{2}mv_{f2}^2=\frac{1}{2}m(v_{f1}^2+v_{f2}^2)
$$





##	Por conservaciones 

$$
mv_o=m(v_{f1}+v_{f2})\\
v_0=v_{f1}+v_{f2}\\
$$


$$
v_o^2=v_{f1}^2+v_{f2}^2
$$

$$
v_o^2=(v_o-v_{f2})^2+v_{f2}^2\\
v_o^2=v_o^2-2v_ov_{f2}+v_{f2}^2+v_{f2}^2\\
0=-2v_o+2v_{f2}\\
v_o=v_{f2}\\
$$




##	Ejemplo

> Dos masas distintas se mueven a velocidades distintas una en contra de la otra y chocan, encuentre las expresiones de las velocidades finales si es un choque elsástico.

 ###	I

$$
p_o=\sum p_i=mv_o+MV_o\\
\\
K_o=\sum K_i=\frac{1}{2}mv_o^2+\frac{1}{2}MV_o^2
$$

###	II

$$
p_f=\sum p_i=mv_f+MV_f\\

\\K_f=\sum K_i=\frac{1}{2}mv_f^2+\frac{1}{2}MV_f^2
$$

###	Por conservaciones

$$
p_o=p_f\\
mv_o+MV_o=mv_f+MV_f
$$

$$
mv_o^2+MV_o^2=mv_f^2+MV_f^2
$$

$$
m(v_0-v_f)=M(V_f-V_o)\\
m(v_o^2-v_f^2)=M(V_f^2-V_o^2)\\
m(v_o-v_f)(v_o+v_f)=M(V_f-V_o)(V_f+V_o)\\
v_0+v_f=V_f+V_o
$$





###	Ejemplo

> Dos partículas de igual masa están, una en reposo, y la otra se mueve a velocidad $v$ en dirección de la primera. Si colisionan de forma completamente inelástica, enuentre la velocidad final del sistema.

###	I

$$
p_o=mv_o\\
p_f=(2m)v_f\\
2mv_f=mv_o\\
v_f=\frac{1}{2}v_o
$$

###	Ejemplo

> Un pez de masa $M$ que se mueve hacia el norte con velocidad $V_o$, se come a otro pez de masa $m$ que se movía hacia el sur con velocidad $v_o$.


$$
p_o=MV_o+mv_o\\
p_f=(M+m)v_f\\
MV_o+mv_o=(M+m)v_f\\
v_f=\frac{MV_o+mv_o}{M+m}
$$

###	Ejemplo

> $M,m$ con $V_o,v_o$ y después del choque la masa grande tiene una velocidad $V_f$ 

> Durante el choque se tiene una pérdida de energía de $11KJ$ 

$$
p_o=MV_o+mv_o\\
p_f=MV_f+mv_f\\
MV_o+mv_o=MV_f+mv_f
$$





##	Vectores

$$
\vec{A}\cdot\vec{B}=AB\cos{\theta}
$$

$$
|\vec{A}\times\vec{B}|=ABsin{\theta}\\
$$

En diercción perpendicular a los dos vectores origniales. 




$$
d_B=\frac{1}{2}at^2\\
d_A=3d_B
$$

$$
x_1(t)=\frac{1}{2}t^2\\
x_2(t)=300-\frac{1}{2}5t^2\\
600=t^2+5t^2=6t^2\\
\sqrt{100}=t=10
$$



$$
v_f=v_o+at=
$$



$$
\sum F_{x1}=m_1a=w\sin\theta-T_1\\
m_2a=T_1-T_2\\
m_3a=T_2
$$

$$
m_2a+m_3a=T_1=a(m_2+m_3)\\
m_1\left(\frac{T_1}{m_2+m_3}\right)=w\sin\theta-T_1\\
T_1(\frac{m_1}{m_2+m_3}+1)=w\sin\theta\\
T_1=\frac{w\sin\theta}{\frac{m_1}{m_2+m_3}+1}=
$$


$$
v_f=v_o+at\\
d=v_ot+\frac{1}{2}at^2
$$

















##	Ejemplos

###	Choque elástico

> *8.44* 
>
> Un bloque de
> 15.0 kg está sujeto a un resorte horizontal muy ligero con constante
> de fuerza de 500.0 N/m, que reposa sobre una mesa horizontal sin fric-
> ción (figura E8.44). De repente, es golpeado por una piedra de 3.00 kg
> que viaja de forma horizontal a 8.00 m/s hacia la derecha, con lo cual
> la piedra rebota horizontalmente a 2.00 m/s hacia la izquierda. Calcu-
> le la distancia máxima que el bloque comprime el resorte después del
> choque.


$$
p_o=mv_o
$$

$$
p_f=mv_f+MV
$$

$$
E_o=\frac{1}{2}MV^2\\
E_f=\frac{1}{2}kx^2
$$

$$
kx^2=MV^2 \tag{1}
$$

$$
mv_o=mv_f+MV\\
V^2=\left(\frac{m(v_o-v_f)}{M}\right)^2\tag{2}
$$

Por (1) y (2):
$$
x=\frac{m(v_o-v_f)}{\sqrt{Mk}}
$$




> *8.95*
>
> $m=50kg$, $v_c=5m/s$, $v_b=3m/s$ a $-37^\circ$, $m_b=15kg$, $h=4m$



**Solo el bloque:**

Tenemos:

En x:
$$
v_x=v_b\cos\theta
$$


En y:
$$
v_{oy}=v_b\sin\theta\\
v_{fy}=\sqrt{v_{oy}^2-2gh}
$$


**a) La velocidad del bloque justo al entrar en el carrito es: $v_{bf}$**



**En el choque:**

Por definición de momento, y considerando solo las componentes en x:
$$
p_{ox}=m_bv_{bx}+m_cv_c\tag{1}
$$

$$
p_{fx}=m_bV+m_cV=(m_b+m_c)V\tag{2}
$$

Por (1), (2) y la conservación del momento:
$$
V=\frac{m_bv_{bx}+m_cv_c}{m_b+m_c}
$$






> *8.31*
>
> $m_a=m_b, \theta=30,\phi=-45,v_{ao}=40,v_{ob}=0$

Por definición del momento:
$$
p_o=mv_{ao}\hat{x}
$$

$$
p_f=m\vec{v_{fa}}+m\vec{v_{fb}}=m[(v_{fa}\cos\theta+v_{fb}\cos\phi)\hat{x}+(v_{fa}\sin\theta+v_{fb}\sin\phi)\hat{y}]
$$



Por conservación del momento, $p_o=p_f\Rightarrow$ $p_{ox}=p_{fx}$ y $p_{oy}=p_{fy}$ :
$$
mv_{ao}=m(v_{fa}\cos\theta+v_{fb}\cos\phi)\\
v_{ao}=v_{fa}\cos\theta+v_{fb}\cos\phi \tag{1}
$$

$$
0=m(v_{fa}\sin\theta+v_{fb}\sin\phi)\\
0=v_{fa}\sin\theta+v_{fb}\sin\phi \tag{2}
$$

De (1) y (2):
$$
v_{ao}=\left(-\frac{v_{fb}\sin\phi}{\sin\theta}\right)\cos\theta+v_{fb}\cos\phi\\
v_{ao}=v_{fb}\left[\left(-\frac{\sin\phi}{\sin\theta}\right)\cos\theta+\cos\phi\right]\\
$$

$$
v_{fb}=\frac{v_{ao}}{\left(-\frac{\sin\phi}{\tan\theta}\right)+\cos\phi}
$$

$$
v_{fa}=-\frac{v_{fb}\sin\phi}{\sin\theta}
$$







$$
K_o=\frac{1}{2}mv_{oa}^2\\
K_f=\frac{1}{2}m[v_{fa}^2+v_{fb}^2]\\
$$

$$
1-\frac{K_f}{K_o}
$$

$$
K_o-1\\
K_f- ?
$$





##	Identidad importante:

$$
\cos^2\alpha+\sin^2\alpha=1
$$

