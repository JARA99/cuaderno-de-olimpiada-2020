#	Dinámica de una partícula

##	Cosas importantes a tomar en cuenta

* Asociar un sistema de referencias al objeto en cuestión (Diagrama de Cuerpo Libre)
* Dibujar los vectores de fuerza
* Sacar componentes de los vectores no alineados con los ejes
* Realizar la suma de fuerzas en cada eje por separado
* Aplicamos la segunda ley de Newton “$F=ma$” que aplicado a cada eje lo podemos escribir como: $F_x=ma_x$, y $F_y=ma_y$.



##	Datos clave para problemas específicos

###	En el vacío, g=0

* Sin fuerzas externas, no existe fuerza aplicada al objeto.
* Si un objeto se mueve a velocidad constante (incluye a los objetos en reposo), la suma de fuerzas en $x$ y en $y$ es igual a cero.
* Si un objeto tiene aceleración $\vec{a}=(a_x,a_y)$, la suma de las fuerzas es igual a $f_x=ma_x$ y $f_y=ma_y$ 



###	En la superficie de la tierra

* Sin fuerzas externas, sabemos que existe al menos el peso $w=mg$ como una fuerza aplicada al objeto.
* Si el objeto está en reposo sobre una superficie, entonces sobre el actúa el peso, una fuerza normal que es perpendicular a la superficie, y una fuerza de fricción que es paralela a la superficie y es proporcional a la fuerza normal.



###	Cuando hay más de un objeto en análisis

* Trabajamos cada objeto por separado, esto es: realizar un DCL distinto para cada objeto, y realizar una suma de fuerzas sobre cada objeto. 
* Buscar que datos tienen en común, o como se relacionan los objetos.
* Cuando los objetos se mueven juntos, comparten la aceleración.

##	Fuerzas comunes

###	Fuerza normal

Es una fuerza que aparece cuando el objeto en cuestión está en contacto con una superficie. La fuerza normal es perpendicular a la superficie de contacto, y comúnmente se ajusta al sistema acercándolo al equilibrio.



###	Fuerza de fricción

Es una fuerza que aparece cuando el objeto está en contacto con una superficie. Esta es proporcional a la fuerza normal y paralela a la superficie. Y está dada por: $F_f=\mu N$.

Se denomina $\mu$ al coeficiente de fricción, que no es más que la constante de proporcionalidad entre la fuerza de fricción y la fuerza normal. En física, entre dos materiales aparecen dos valores de $\mu$ distintos, uno cuando el objeto está en reposo (en relación a la superficie), este se llama coeficiente de fricción estática $\mu_s$, y cuando el objeto está en movimiento (en relación a la superficie), este se llama coeficiente de fricción cinética $\mu_k$. 

En los materiales que conocemos, $\mu_s>\mu_k$.



###	Tensión

Es una fuerza, muy parecida a una fuerza de empuje, pero en este caso, es (comúnmente) realizada por un cable, cuerda, cadena, etc. halando el objeto.



> Ejemplo:
>
> Calcule la fuerza de empuje para que un objeto ubicado a $5m$ de altura alcance un desplazamiento horizontal de $5m$ después de haber sido empujado horizontalmente una distancia de $3m$. (sin fricción). La masa del objeto es $5kg$

Calculamos la velocidad con la que tiene que partir para alcanzar 5m de desplazamiento horizontal.



| x    | y        |
| ---- | -------- |
| d=5m | a=9,8m/s |
| t=   | v_o=0    |
| v=   | h=5m     |
|      | t=       |
|      |          |

En y

$h=v_ot+\frac{1}{2}at^2=\frac{1}{2}at^2$

$t=(2h/a)^{\frac{1}{2}}$

En x

$d=tv$

$v=d/t=d\sqrt{a/2h}$





Pasando al desplazamiento horizontal

$v_o=0$

$v_f=v=d\sqrt{a/2h}$

$d=3m$

$a=\frac{v_f^2-\cancelto{0}{v_o^2}}{2d}=\frac{v_f^2}{2d}$ 



Por último, calculando la fuerza de empuje.

$\sum F_x=F=ma_x=\frac{mv_f^2}{2d}$





##	Ejemplos

4.34)

*Encontrando la aceleración:*
$$
2ad=v_f^2-v_o^2\\
$$

$$
a=\frac{v_f^2-v_o^2}{2d}\\ \tag{1}
$$

*Encontrando la fuerza:*

Por la segunda ley de newton:
$$
\sum F=ma
$$
La única fuerza aplicada a la bala es la de del árbol $F_A$. Por tanto $\sum F= F_A$

De esto:
$$
F_A=ma\tag{2}
$$
Sustituyendo $(1)$ en $(2)$:
$$
F_A=m\left(\frac{v_f^2-v_o^2}{2d}\right)\\~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\square
$$
4.43)

$m$ es la masa pequeña, y $M$ es la masa grande

b)
$$
\sum F = T=ma \tag{1}
$$
d)
$$
\sum F_x=F-T=F-ma=Ma\\
F=Ma+ma=a(m+M)
$$
5.86)
$$
\sum F_{xM}=T-w \sin(30)=T-\frac{1}{2}Mg \tag{1}=Ma
$$

$$
\sum F_{xm}=-T+mg\sin(53.1)=ma\tag{2}
$$

$$
T-\frac{Mg}{2}=Ma\\
-T+mg\sin(53.1)=ma\\
$$

Sumando $(1)$ y (2)
$$
-\frac{Mg}{2}+mg\sin(53.1)=Ma+ma\\
\frac{-\frac{Mg}{2}+mg\sin(53.1)}{M+m}=a=-0.65m/s^2
$$
