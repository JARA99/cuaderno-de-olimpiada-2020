#	 Electromagnetismo

La fuerza electromagnética es una de las cuatro fuerzas de la naturaleza. Estas son: Gravitacional, Electromagnética, Nuclear débil, Nuclear fuerte.



##	Unidad fundamental del electromagnetismo

La unidad fundamental del electromagnetismo es la carga $q$ medida en Coulomb $[C]$. Una partícula puede tener carga positiva o negativa. Cargas opuestas se atraen. La carga fundamental es $e=1.6E-19$. El electrón tiene carga $-e$ y el protón tiene carga $+e$.



La antipartícula del electrón es llamada *positrón*, y es distinta al protón, ambas tienen carga $e$ pero la masa del positrón es igual a la del electrón, mientras que la masa del protón es mucho mayor.

* Masa del electrón $=9.1E-31$
* Masa del protón $1.67E-27$ 





La fuerza de atracción entre dos cargas está dada por:


$$
F_{BA}=\frac{1}{4\pi \epsilon_0}\frac{q_Aq_B}{r^2}\hat{r}
$$

* Donde $\epsilon_0=8.8542E-12$ es la permitividad eléctrica del vacío.


$$
|F|=\frac{1}{4\pi\epsilon_0}\frac{|q_A||q_B|}{r^2}
$$
Analgía
$$
F_G=-\frac{G}{4\pi}\frac{Mm}{r^2}\hat{r}
$$


##	Campo eléctrico

Para una carga de prueba $q_o$ que puede estar ubicada en cualquier punto $\vec{r}$ del espacio, tenemos:


$$
\vec{F}(\vec{r})=\frac{1}{4\pi\epsilon_0}\frac{q_oQ}{r^2}\hat{r}
$$

$$
\frac{\vec{F}(\vec{r})}{q_o}=\frac{1}{4\pi\epsilon_o}\frac{Q}{r^2}\hat{r}=\vec{E}~\alpha~\frac{\hat{r}}{r^2}\\
\frac{\vec{F}}{q_o}=\vec{E}
$$

$$
\vec{F}=q\vec{E}
$$



El campo de una carga positiva es radialmente hacia afuera de la carga, y el campo de una carga negativa es radialmente hacia adentro de la carga.



Las líneas de campo eléctrico apuntan en la dirección en la que se movería una carga positiva.

##	Ejemplo - ONC-2020-1

$$
t=4E-8s\\
v_o=4E6~~\hat{x}~~m/s \\
h=0.03m \hat{y}\\
q=-e
$$

$$
\vec{F}=-F\hat{y}=-e\vec{E}=m\vec{a}
$$


$$
h=\cancelto{0}{v_{oy}t}+\frac{1}{2}at^2\\\vec{a}=-\frac{2h}{t^2}\hat{y}
$$

$$
\hat{E}=\frac{2hm}{t^2e}\hat{y}
$$




##	Ejemplo - ONC-2020-1




$$
F_y=0=-mg+T\cos\theta+QE\sin\phi\\

F_x=0=-QE\cos\phi+T\sin\theta
$$

$$
E(\phi)
$$

$$
Tcos\theta=mg-QE\sin\phi\\
T\sin\theta=QE\cos\phi\\
\text{de esto:}\\
\tan\theta\left(mg-QE\sin\phi\right)=QE\cos\phi\\
mg\tan\theta=QE(\cos\phi+\tan\theta\sin\phi)\\
E(\phi)=\frac{mg\tan\theta}{Q(\cos\phi+\tan\theta\sin\phi)}
$$

Entonces $E(\phi)$ se minimiza cuando $\cos\phi+\tan\theta\sin\phi$ es máximo, podes graficar en geogebra esa función con el valor de $tan\theta$ del problema y pues encontrás donde está el máximo.



##	Resumen

####	Fuerza eléctrica entre dos cargas puntuales

$$
F=\frac{1}{4\pi\epsilon_o}\frac{q_1q_2}{r^2}
$$



####	Definición de campo eléctrico

El campo eléctrico aparece en el espacio en presencia de carga.

Las lineas de campo representan la dirección en la que una carga positiva se movería debido a la fuerza eléctrica.

La fuerza eléctrica sobre una carga $q$ debido a un campo externo $E$ está dada por:
$$
F=qE
$$
**De esto puede venir muy poco**





##	Mini-introducción a los fundamentos de circuitos

###	Potencial eléctrico $V$

Recordatorio:
$$
\frac{dU}{dx}=F\\
$$


Que tal si hacemos algo parecido, pero esta vez lo hacemos con el campo $E$.
$$
\phi=V,~~\text{es un escalar}\\
\nabla\phi=\nabla V=E\\
Donde:\\
\nabla=(\frac{d}{dx},\frac{d}{dy},\frac{d}{dz})
$$


Lo que a nosotros nos interesa, es un cambio de potencial. Un cambio de energía potencial $\Delta U$ conlleva consigo un cambio de potencial eléctrico $\Delta V$. A este cambio de potencial eléctrico se le llama voltaje, y usualmente se escribe solo $V$. 



##	Circuitos

###	Ley ohm

$$
I=\frac{V}{R}
$$

* Corriente $I=\frac{dq}{dt}$ 
* Voltaje (Cambio de Potencial)
* Resistencia, el impedimento de un material a que las cargas se muevan.



Hay dos formas en las que resistencias pueden aparecer en un circuito:



#####	En paralelo

Es cuando las resistencias comparten ambas terminales.

![](Img./paralelo.jpg)
$$
\frac{1}{R}=\frac{1}{r_1}+\frac{1}{r_2}+\cdots+\frac{1}{r_n}\\
R=\frac{1}{\frac{1}{r_1}+\frac{1}{r_2}+\cdots+\frac{1}{r_n}}
$$
Para solo dos resistencias:
$$
R=\frac{1}{\frac{1}{r_1}+\frac{1}{r_2}}=\frac{1}{\frac{r_2+r_1}{r_2r_2}}=\frac{r_1r_2}{r_1+r_2}
$$




#####	En serie

Es cuando las resistencias se colocan una seguida de la otra



![](Img./serie.jpg)


$$
R=r_1+r_2+\cdots+r_n
$$


###	Leyes Kirchoff

Links a videos:

* [Mallas](https://www.youtube.com/watch?v=1NC9kGDn7Bg)
* [Nodos](https://www.youtube.com/watch?v=KiKMwEG-l3I)