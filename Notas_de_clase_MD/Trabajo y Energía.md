Trabajo y Energía

##	Trabajo

$$
W=F\cdot s [J] \\
J=kg~ m^2/s^2=Nm
$$

> **Repaso de vectores**
>
> El producto punto puede calcularse como la multiplicación de las magnitudes de los vectores por el coseno del ángulo entre ellos.
>
> $A\cdot B=|A||B|cos(\theta)$


$$
2ad=v_f^2-v_o^2
$$

$$
\begin{align}
	a&=\frac{v_f^2-v_o^2}{2d}\\
	ma&=m\frac{v_f^2-v_o^2}{2d}\\
	Fd&=\frac{m}{2}(v_f^2-v_o^2)\\
	W&=\frac{1}{2}mv_f^2-\frac{1}{2}mv_o^2\\
\end{align}
$$



##	Energía Cinética

$$
K=\frac{1}{2}mv^2
$$

La energía cinética inicial la podemos calcular valuando $K$ en el instante inicial cuando $v=v_o$. Esto es: $K_o=\frac{1}{2}mv_o^2$,  De la misma manera calculamos $K_f=\frac{1}{2}mv_f^2$ 


$$
W=K_f-K_o=\Delta K
$$
El teorema de trabajo y energía indica que el trabajo realizado sobre un objeto es igual a su cambio de energía cinética.



> **Ejemplo:**
>
> Un disco de hockey con masa de 0.160 kg está en reposo en el
> origen $(x=0)$ sobre la pista, que es y sin fricción. En el tiempo (t=0),
> un jugador aplica una fuerza de 0.250 N al disco, paralela al eje x,
> y deja de aplicarla en t=2s. a) ¿Qué posición y rapidez tiene el
> disco en t = 2.00 s? b) Si se aplica otra vez esa fuerza en t = 5.00 s,
> ¿qué posición y rapidez tiene el disco en t = 7.00 s?

$$
\begin{align}
	W=F\cdot d&=\Delta K\\
	Fd&=\frac{1}{2}mv_f^2-\cancelto{0}{\frac{1}{2}mv_o^2}
\end{align}
$$

$d=v_0t+\frac{1}{2}at^2$


$$
Fat^2=mv_f^2\\
Ft^2(\frac{F}{m})=mv_f^2\\
\frac{F^2t^2}{m^2}=v_f^2\\
\frac{Ft}{m}=v_f
$$
**Ejemplo**

> Un balón de fútbol sóquer de 0.420 kg se mueve inicialmen-
> te con rapidez de 2.00 m>s. Un jugador lo patea, ejerciendo una fuerza
> constante de 40.0 N en la dirección del movimiento del balón. ¿Du-
> rante qué distancia debe estar su pie en contacto con el balón para
> aumentar la rapidez de éste a 6.00 m>s?

$m$, $v_0$, $v_f$, $F$.


$$
W=\Delta K\\
F\cdot d=\frac{1}{2}mv_f^2-\frac{1}{2}mv_0^2
$$





#	Potencia

$$
P=\frac{W}{\Delta t}
$$




$$
\begin{align}P&=\frac{W}{\Delta t}\\&=\frac{F\cdot d}{\Delta t}\\&=F\cdot \frac{d}{\Delta t}\\&=F\cdot v\\\\P&=F\cdot v\end{align}
$$







#	Energía Potencial

$$
W_{grav}=wh=mgh
$$

$$
U_{grav}=U=mgh
$$



#	Conservación de la energía

##	Energía mecánica:

$$
E=K+U\\
$$

##	Conservación de la energía:

$$
\Delta E=W_{F_{no~conservativas}}\\
E_f-E_o=W_{Fn}\\
E_o=E_f-W_{Fn}
$$



###	Fuerzas conservativas

Fuerzas cuyo trabajo solo depende de los puntos inicial y final, y no depende de la trayectoria.

El trabajo de una trayectoria cerrada, que empieza y termina en el mismo punto, se cancela.

* El peso



###	Fuerzas no Conservativas

Fuerzas cuyo trabajo si depende de la trayectoria (recorrido)



* Fuerza de fricción





#	Energía potencial elástica

##	Fuerza de un resorte

$$
F_{R}=k~~\Delta x
$$

##	La energía potencial elástica

$$
U_E=\frac{1}{2}kx^2
$$

> Imagine que le piden diseñar un resorte que confiera a un satélite
> de 1160 kg una rapidez de 2.50 m>s relativa a un transbordador espa-
> cial en órbita. El resorte debe imprimir al satélite una aceleración má-
> xima de 5.00g. La masa del resorte, la energía cinética de retroceso del
> transbordador y los cambios en la energía potencial gravitacional serán
> despreciables. a) ¿Qué constante de fuerza debe tener el resorte?
> b) ¿Qué distancia debe comprimirse el resorte?

$$
F_{max}=ma_{max}\\
$$


$$
kx_{max}=ma_{max}\tag{1}
$$

$$
U_{I}=K_{II}\\
\frac{1}{2}kx_{max}^2=\frac{1}{2}mv^2
$$

$$
\frac{1}{2}kx_{max}^2=\frac{1}{2}mv^2\tag{2}
$$

Por (1) y (2):
$$
\frac{1}{2}k\left(\frac{ma_{max}}{k}\right)^2=\frac{1}{2}mv^2
$$





##	Ejemplo

> Un carrito
> de un juego de un parque de diver- Figura 7.32 Problema 7.46.
> siones rueda sin fricción por la vía
> A
> de la figura 7.32, partiendo del re-
> poso en A a una altura h sobre la
> B
> h
> base del rizo. Trate el carrito co-
> C R
> mo partícula. a) ¿Qué valor mí-
> nimo debe tener h (en términos de
> R) para que el carrito se desplace por el rizo sin caer en la parte supe-
> rior (el punto B)? b) Si h 5 3.50R y R 5 20.0 m, calcule la rapidez,
> aceleración radial y aceleración tangencial de los pasajeros cuando
> el carrito está en el punto C, en el extremo de un diámetro horizon-
> tal. Haga un diagrama a escala aproximada de las componentes de
> la aceleración.


$$
E_I=E_{III}\\
\frac{7}{2}mgr=mgr+\frac{1}{2}mv^2
$$

$$
\begin{align}
	\frac{7}{2}mgr-mgr&=\frac{1}{2}mv^2\\
	\frac{5}{2}mgr&=\cdots\\
	\frac{2}{m}*\frac{5}{2}mgr&=v^2\\
	\sqrt{5gr}&=v
\end{align}
$$


$$
a_c=\frac{v^2}{r}\\
a_c=\frac{5gr}{r}\\
a_c=5g
$$



$$
\sum F_y=w=mg=ma_y
$$

$$
a_y=mg/m=g
$$




$$
E_I=mgh\\
E_{II}=mg(2r)+\frac{1}{2}mv^2\\
\\
mgh=mg(2r)+\frac{1}{2}ma_cr\\
h=2r+\frac{1}{2}\frac{a_cr}{g}\\
h=2r+\frac{1}{2}r=\frac{3}{2}r
$$




#	Fuerza en términos de energía potencial


$$
F_A=\frac{d}{dx}[U_A(x)]
$$



$$
U_G(y)=mgy\\
\frac{d}{dy}U_G(y)=mg=F_G=W
$$



$$
U=mgh+\frac{1}{2}kx^2
$$



$$
U(x)=-\frac{C_6}{x^6}=-C_6x^{-6}\\
F=\frac{dU}{dx}=-6(-C_6x^{-6-1})\\=6C_6x^{-7}\\=\frac{6C_6}{x^7}
$$





#	Resumen

##	Trabajo

$$
W=F\cdot d
$$



##	Energía cinética

$$
K=\frac{1}{2}mv^2
$$

$$
W=\Delta K=K_f-K_o
$$



##	Energía potencial


$$
U=mgh=mgy
$$

$$
W_G=F_G\cdot \Delta s=-mg\Delta s\cos\theta=-mg\Delta h\\
\\
W_G=-(mgh_f-mgh_o)\\
W_G=-(U_f-U_o)\\
W_G=-\Delta U
$$

##	Conservación de la energía

$$
W=\Delta K\\
W=-\Delta U\\
\Delta K=-\Delta U\\
\Delta K+\Delta U=0\\
K_f-K_o+U_f-U_o=0\\
K_f+U_f=K_o+U_o\\
E_f=E_o
$$



##	Energía potencial elástica

###	Fuerza del resorte

$$
F_r=kx
$$

### $U_e$

$$
U_e=\frac{1}{2}kx^2
$$



##	Conservación de la energía

Sea $W_{NoC}$ el trabajo efectuado por fuerzas no conservativas, entonces tenemos:


$$
E_f=E_o+W_{NoC}
$$

###	Energía mecánica

$$
E=K+U
$$





##	Ejemplo

> Un estudiante propone un diseño para
> una barrera contra choques de automóviles consistente en un resorte
> con masa despreciable capaz de detener una vagoneta de 1700 kg que
> se mueve a 20.0 m>s. Para no lastimar a los pasajeros, la aceleración
> del auto al frenarse no puede ser mayor que 5.00g. a) Calcule la cons-
> tante de resorte k requerida, y la distancia que el resorte se comprimirá
> para detener el vehículo. No considere la deformación sufrida por el
> vehículo ni la fricción entre el vehículo y el piso. b) ¿Qué desventajas
> tiene este diseño?

$$
E_o=K_o+U_o=\frac{1}{2}Mv^2
$$

$$
E_f=K_f+U_f=0+\frac{1}{2}kx^2
$$


$$
a<a_{max}\tag{3}
$$

$$
\sum F=Ma<Ma_{max}
$$

$$
kx=Ma\tag{1}
$$

$$
Mv^2=kx^2 \tag{2}
$$



Por (1) y (2):
$$
Mv^2=k\left(\frac{Ma}{k}\right)^2\\
\text{De (3)}\\
k=\frac{Ma^2}{v^2}<\frac{Ma_{max}^2}{v^2}=
$$

$$
x=\frac{Ma}{k}=\frac{v^2}{a}>\frac{v^2}{a_{max}}
$$
