#	Resolución de Dudas de problemas

##	Ecuación de Movimiento

Un objeto tiene delimitada su posición por la siguiente ecuación: $x(t)=5+6t+8t^2$. Determine su posición, velocidad y aceleración en los tiempos $t_1=8s$, $t_2=10s$.
$$
x(8)=5+6*8+8*8^2=565\\
x(10)=5+6*10+8*10^2=865
$$

$$
x(t)=x_o+v_ot+\frac{1}{2}at^2\\
x(t)=5+6t+8t^2\\
$$

$$
x_o=5\\
v_o=6\\
\frac{1}{2}a=8
$$

$$
a=\frac{v_f-v_o}{t}
$$







$x(t)=5+6t+8t^2$ 

$t_0=3s$, $t_f=5s$ 

$x(3)=5+6*3+8*3^2=x_0$

$x(5)=5+6*5+8*5^2=x_f$



$x(t)=...$

$t_0=...$, $t_f=...$

$x(t_o)=x_o$

$x(t_f)=x_f$



*Solución con cálculo:*
$$
\begin{align}
v&=\frac{\Delta x}{\Delta t}\\
v&=\frac{x_f-x_o}{t_f-t_o}\\
v&=\frac{x(t_f)-x(t_o)}{t_f-t_o}\\\\
v&=\lim_{\Delta t\rightarrow0}\frac{x(t_f)-x(t_o)}{\Delta t}\\\\

v&=\frac{dx}{dt}\\
&=\frac{\Delta x}{\Delta t}
\end{align}
$$


$x(t)=5t^0+6t+8t^2$, ahora encontramos la velocidad
$$
v=\frac{dx}{dt}=\frac{d}{dt}[x(t)]=0*5t^{0-1}+1*6t^{1-1}+2*8t^{2-1}=0+6+16t\\

v(t)=6+16t
$$

$$
v(8)=6+16*8=\\
v(10)=6+16*10=166\\
$$





$v(t)=6+16t$ 


$$
a=\frac{\Delta v}{\Delta t}\\
a=\frac{dv}{dt}\\\\
a(t)=\frac{d}{dt}{[6+16t]}=16
$$




$x(t)=8t^6+7t^4+19t^2+73.82$
$$
v(t)=\frac{d}{dt}[x(t)]=48t^5+28t^3+38t\\
a(t)=\frac{d}{dt}[v(t)]=240t^4+84t^2+38
$$




###	Chivo:

Dada una expresión de la forma $x(t)$, siempre podemos encontrar $v(t)$, y $a(t)$ como:
$$
v(t)=\frac{d}{dt}x(t)\\\\
a(t)=\frac{d}{dt}v(t)
$$

$$
\int v~dt=\int dx
$$


$$
\int a~dt=\int dv
$$


#	Como encontramos la velocidad instantánea si no tenemos una función x(t)




$$
a(t)=\frac{d}{dt}v(t)
$$

$$
d=tv
$$

###	Chivo

En una gráfica, el área bajo la curva, dependiendo de que sean los ejes, podría representar, desplazamiento, velocidad.

Gráfica t-v $\rightarrow$ d

Gráfica t-a $\rightarrow$ v



#	Problema 5.31



#	Problema 4.15

$$
F=A+Bt^2=ma
$$

$$
\begin{align}
a&=\frac{A+Bt^2}{m}\\
&=\frac{A}{m}+\frac{B}{m}t^2\\\\

\end{align}
$$




$$
F(t)=A+Bt^2\\
F(0)=A+\cancelto{0}{Bt^2}=100N\\
A=100N\\
F(2)=A+4s^2B=150N\\\\

\text{Sustituyendo (1) en (2)}\\

B=\frac{150N-100N}{4s^2}=12.5N/s^2
$$



$$
\begin{align}
\sum F_y&=F(0)-w\\
&=(A+B0^2)-mg\\
&=valor
\end{align}
$$

$$
\begin{align}\sum F_y&=F(3)-w\\&=(A+B3^2)-mg\\&=valor\end{align}
$$

$$
\begin{align}\sum F_y&=F(3)-\cancelto{0}{w}\\&=(A+B3^2)\\&=valor\end{align}
$$







#	Problema 6.17

$$
E_{I}=K_{I}+\cancelto{0}{U_I}=\frac{1}{2}mv^2
$$

$$
E_{II}=\cancelto{0}{K_{II}}+mgh=mgh
$$


$$
\Delta E=W_{F_f}\Rightarrow\\
E_I=E_{II}+W_F
$$

$$
\frac{1}{2}mv^2=mgh+N\mu_kd\\
\frac{1}{2}mv^2=mgh+mg\cos\alpha\mu_k(\frac{h}{\sin{\alpha}})\\
v=\sqrt{2gh+\frac{2g\mu_kh}{\tan{\alpha}}}
$$


$$
v=\sqrt{2gh+\frac{2g\mu_kh}{\tan{\alpha}}}\\
v=\sqrt{2gh}
$$



$$
\sin\alpha=\frac{Op}{Hip}\\

Hip=\frac{Op}{sin\alpha}\\

\sin/\cos=\tan\\

\cos/\sin=1/\tan
$$







##	Aceleración instantánea

###	Aceleración

$$
a=\frac{\Delta v}{\Delta t}=\frac{v_f-v_o}{\Delta t}
$$




$$
a_{inst}=\lim_{\Delta t\rightarrow0}\frac{\Delta v}{\Delta t}\\

a_{inst}=\frac{dv}{dt}
$$



$$
r(t)=\\
v(t)=\\
a(t)=
$$

$$
a(t_{deseado})\\
a(t)=\frac{dv}{dt}\\
a(t)=\frac{d}{dt}v=\frac{d}{dt}\frac{dx}{dt}=\frac{d^2x}{dt^2}
$$




##	2.88

$$
T=t_{fall}+t_s
$$


$$
h=\cancelto{0}{v_ot}+\frac{1}{2}gt_{fall}^2\\

t_{fall}=\sqrt{\frac{2h}{g}}
$$

$$
h=t_sv_s\\
t_s=\frac{h}{v_s}
$$


$$
T=\sqrt{\frac{2h}{g}}+\frac{h}{v_s}\\
h=\alpha^2\\
\alpha=\sqrt{h}\\
T=\sqrt{\frac{2\alpha ^2}{g}}+\frac{\alpha^2}{v_s}\\
0=\alpha\sqrt{2/g}+\alpha^2/v_s-T
$$



$$
\hat{i}\times\hat{j}=\hat{k}\\
(1,0,0)\times(0,1,0)=(0,0,1)
$$

$$
\begin{matrix}
i&j&k\\
1 &0 & 0\\
0&1&0
\end{matrix}=[(1*1)-(0*0)]k-[(1*0)-(0*0)]j=k
$$





$$
\begin{matrix}
i&j&k\\
\end{matrix}
$$














##	Producto punto - Producto escalar

$$
\vec{A}\cdot\vec{B}=\lambda
$$

Donde $\lambda$ es un escalar que proviene de sumar las multiplicaciones de componente a componente. Ej: $(a,b,c)\cdot(e,f,g)=ae+bf+cg$ 

Donde $\lambda$ es $|A||B|cos\theta$ donde $\theta$ es el ángulo entre ellos.



##	Producto cruz - Producto vectorial

$$
\vec{A}\times\vec{B}=\vec{C}
$$

Donde $\vec{C}$ es igual al determinante de la matriz donde cada fila corresponde a los vectores $(i,j,k),\vec{A},\vec{B}$ respectivamente.



Donde $|C|$ es igual a $|A||B|\sin\theta$ donde $\theta$ es el ángulo entre los dos vectores. Y la dirección es un vector perpendicular al plano formado por $\vec{A}$ y $\vec{B}$  













##	Chang

$$
T_y=T\sin\theta =mg\\
T=\frac{mg}{\sin\theta}
$$



##	P7.17 Resnik

$$
\sum \vec{F}=-F_c\hat{r}=-T\hat{r}
$$

$$
ma_c=m\frac{v^2}{r}=F_c=T
$$

