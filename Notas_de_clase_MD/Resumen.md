#	Resumen

##	Vectores

* Definición de vector
* Suma y resta de vectores
* Multiplicación por escalar
* Producto cruz y punto
* Distintos sistemas de referencias: rectangular o cartesiano, polar



##	Conversiones

Por cada unidad ($1$) de una medición en $a$, tengo una cantidad $x$ en una medición $b$



####	Ejemplo

> $1.3h\rightarrow min$
>
> Tengo que por cada hora hay 60 minutos.
> $$
> 1.3h\cdot \frac{60min}{1h}=78min
> $$
> 



#### Ejemplo

> $300m/s\rightarrow m/min$
>
> Por cada minuto hay 60 segundos.
> $$
> 300\frac{m}{s}\cdot \frac{60s}{1min}=18000m/min
> $$
> 





##	Cinemática

$$
d=\Delta r=tv
$$

$$
v_f=v_o+at
$$

$$
d=\Delta r=v_ot+\frac{1}{2}at^2
$$

$$
d=\Delta r=v_ft-\frac{1}{2}at^2
$$

$$
2ad=v_f^2+v_o^2
$$



##	Dinámica

$$
F=ma\\
\sum F_i=ma_i~~~~~i\in \{x,y,z\}
$$



##	Trabajo y energía

$$
W=F\cdot d
$$

$$
W_{net}=F_{net}\cdot d=\sum F\cdot d
$$

$$
K=\frac{1}{2}mv^2
$$

$$
U_G=mgh=mgy
$$

$$
U_E=\frac{1}{2}kx^2
$$

$$
E=K+U
$$

$$
\Delta E=W_{NoC}\\
\Longrightarrow\\
E_f=E_o+W_{NoC}
$$

