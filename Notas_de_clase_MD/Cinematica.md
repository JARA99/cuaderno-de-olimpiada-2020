#	Cinemática de una partícula

##	MRU sobre el eje $x$

**Por Definición** La velocidad media se define como el cambio de posición en un tiempo. 

$$
v=\frac{x_f-x_o}{\Delta t} \tag{1}
\label{vmx}
$$

**Donde:**

* $v$ es la velocidad
* $x_o$ es la posición inicial
* $x_f$ es la posición final
* $\Delta t$ el cambio de tiempo

**Ejemplo:**

> Una pelota rueda sobre una mesa, desde 5cm de su extremo
> izquierdo, hasta 5cm de su extremo derecho en 7s. Si la mesa
> tiene un metro de ancho, cual es la velocidad media de la pelota.

Por la ecuación $\ref{vmx}$ tenemos que:

​							*Con el origen en el extremo izquierdo*
$$
v=\frac{x_f-x_o}{\Delta t}=\frac{95-5}{7}=\frac{90}{7}\approx 12.85~cm/s \text{ hacia la derecha de la mesa}
$$
​							*Con el origen en el centro*
$$
v=\frac{x_f-x_o}{\Delta t}=\frac{45-(-45)}{7}=\frac{90}{7}\approx 12.85~cm/s~\hat{x} ~ o ~ \hat{i}
$$


* **Desplazamiento:** Es el cambio de posición. 
* **Recorrido:** La suma de toda la trayectoria.
* **Rapidez:** La magnitud de la velocidad.

**Ejercicio:**

> Un carrito a control remoto viaja a con una rapidez $v$ en línea
> recta, en $t$ segundos, ¿cuánto se habrá desplazado?

Nos piden: $d=x_f-x_o$, de la ecuación ($\ref{vmx}$) despejamos $d$:

​		
$$
\begin{align}
	&v=\frac{x_f-x_o}{\Delta t}=\frac{d}{\Delta t}\\ \\
	\text{despejando }d:\\ \\
	&d=v\Delta t
\end{align}
$$

> Si un extremo de una cuerda de largo $l$ se ata a este carro, y
> el otro extremo se ata a un eje, de modo que el carrito
> comienza a dar vueltas en círculos:
>
> 1. ¿Cuál será su recorrido después de dar 2 vueltas exactas?
> 2. ¿Cuál será su desplazamiento después de dar 2 vueltas exactas
> 3. ¿Cuál será su velocidad media después de dar 2 vueltas
>    exactas?
> 4. Repita estos incisos para media vuelta, y una vuelta y un
>    cuarto.
> 5. ¿Cuál será su rapidez?

1. $2P$ donde $P$ es el perímetro del círculo. $2\pi l=P$, entonces el recorrido es $4\pi l$.
2. El desplazamiento es cero.
3. $0/\Delta t=0$ la velocidad es cero.
4. Oral
5. $v$





**Ejercicio de Zemansky**

2.1. 

a) $v=\frac{1000-63}{4.75}\approx 197.26m/s$  

b) $v=\frac{1000-0}{5.9}\approx 169.49m/s$

c) $v=\frac{63-0}{1.15}\approx 54.78m/s$

d) $v_a/v_c=3.60$



2.3.

v=29.17m/s

$t_1=8400s$

$d=v\Delta t=245,028m$

$\Delta t_2=d/v=245,028/19.44=12,607s$

$t_2-t_1=4,207s$



## MRU en un plano o volumen

*¿Qué pasa en más de una dimensión?*
$$
v=\frac{\vec{r_f}-\vec{r_o}}{\Delta t} \tag{2} \label{vm}
$$
**Ejemplo**

> Un objeto se mueve de una posición (1, 3) hacia el origen en 1.3s.
> Calcule su velocidad media

$$
v=\frac{r_f-r_o}{\Delta t}=\frac{(0,0)-(1,3)}{1.3}=\frac{1}{1.3}(-1,-3)=(-\frac{1}{1.3},-\frac{3}{1.3})=-(0.77,2.31)
$$

$\mid v \mid = \sqrt{0.77^2+2.31^2}=2.435$ 

---

####	Recordatorio de vectores:

$$
\hat{n}=\frac{\vec{n}}{\mid n \mid}
$$

---

**Ejemplo**

> Un meteorito se mueve en línea recta en el espacio, desde las
> coordenadas $(3, 5, 7) ∗ 10 ^4 m$ hasta las coordenadas
> $(2, 4, 7) ∗ 10 ^4 m$ en un minuto. Calcule su rapidez. Luego
> encuentre una expresión para un vector unitario $\hat{n}$ en la
> dirección de la velocidad.

De nuevo tomamos la ecuación $\ref{vm}$ para realizar este problema. 
$$
\begin{align}
	\vec{v}&=\frac{\left[(2,4,7)-(3,5,7)\right]*10^4}{60}\\
	&=\frac{\left[-1,-1,0\right]*10^4}{60}\\
	&=\frac{\left[-1,-1,0\right]*10^3}{6}\\
	&=\left(-\frac{1}{6},-\frac{1}{6},0\right)*10^3\\\\
	&&\text{Por tanto}\\\\
	\mid v\mid&=\sqrt{\frac{1}{6^2}+\frac{1}{6^2}+0^2}*10^3\\
	&=\frac{1}{6}\sqrt{2}*10^3 m/s\\\\
	&&Y\\\\
	\hat{n}=\hat{v}&=\frac{\left(-\frac{1}{6},-\frac{1}{6},0\right)}{\frac{1}{6}\sqrt{2}}\\
	&=\left(-\frac{1}{\sqrt{2}},-\frac{1}{\sqrt{2}},0\right)\\
	&=\left(-\frac{\sqrt{2}}{2},-\frac{\sqrt{2}}{2},0\right)
\end{align}
$$

![](Img./G1.png)

###	MRUV 

De la definición de aceleración.
$$
a=\frac{v_f-v_o}{\Delta t} \tag{3} \label{a}
$$

$$
v=[\frac{m/s}{s/1}]=[m/s^2]
$$



**Ejemplo**

> Calcule la aceleración de un carro que puede ir de cero a cien
> en siete segundos. Compare su respuesta con la aceleración
> de la gravedad.

$$
a=\frac{100}{7}\approx14.29m/s^2
$$

$g=9.8$ 
$$
a/g=\frac{100}{7g}=\frac{100}{68.6}\approx 1.45 \\

a=1.45 g
$$


**Ejemplo**

> Si un objeto cae con una aceleración de $9.8m/s ^2$ debido a la
> gravedad, determine en $m/s$ y en $ft/s$, cual será la velocidad
> del objeto después de caer durante dos segundos.

$$
v_f=v_o+a\Delta t=0+9.8*2=19.6 m/s
$$





###	Ecuaciones derivadas de las principales


$$
\bar{v}=\frac{x_f-x_o}{\Delta t}
$$

$$
a=\frac{v_f-v_o}{\Delta t}
$$

$$
\bar{v}=\frac{v_o+v_f}{2}
$$



####	Posición - Aceleración - Velocidad inicial - Tiempo:

$$
d=v_0 \Delta t+\frac{1}{2}a\Delta t^2
$$

***Demostración:***
$$
\begin{align}
	r_f-r_0=d&=\bar{v}\Delta t\\
	d&=\left(\frac{v_o+v_f}{2}\right)\Delta t \tag{1}\\\\
	v_f=v_o+a\Delta t \tag{2}\\
	\text{Por (1) y (2)}\\
	
	d&=\left(\frac{v_o+(v_o+a\Delta t)}{2}\right)\Delta t \\
	d&= v_o\Delta t+\frac{1}{2}a\Delta t^2 \\
	&&\square
\end{align}
$$

####	Posición - Aceleración - Velocidad final - Tiempo

$$
d=v_f\Delta t-\frac{1}{2}a\Delta t ^2
$$



####	Posición - Aceleración - Velocidad inicial y final

$$
2 a(x_f-x_o)=v_f^2-v_o^2\\
$$

***Demostración***:
$$
\begin{align}
a\Delta t= v_f-v_o \tag{1}\\
2\bar{v}=v_o+v_f \tag{2}\\\\

\text{Multiplicando (1) y (2)}\\\\

2\bar{v}a\Delta t=(v_f-v_o)(v_f+v_o)\\
2 a d=v_f^2-v_o^2\\
2 a(x_f-x_o)=v_f^2-v_o^2\\
&&\square
\end{align}
$$


###	Todas las ecuaciones de cinemática:

**Definición de velocidad**
$$
v=\frac{x_f-x_0}{\Delta t}
$$
**Definición de aceleración**
$$
a=\frac{v_f-v_o}{\Delta t}
$$
**Posición - Aceleración - Velocidad inicial - Tiempo**
$$
d=v_0 \Delta t+\frac{1}{2}a\Delta t^2
$$
**Posición - Aceleración - Velocidad final - Tiempo**
$$
d=v_f\Delta t-\frac{1}{2}a\Delta t ^2
$$
**Posición - Aceleración - Velocidad inicial - Velocidad final**
$$
2 a(x_f-x_o)=v_f^2-v_o^2\\
$$




###	Alcances y encuentros



**Resolver por separado cada objeto**
$$
r_{objeto1}(t)
$$

$$
r_{objeto2}(t)
$$

$r_{objeto1}(t)=r_{objeto2}(t)$





###	Ejemplos de problemas

**Ejemplo 1:** Alcance

> Un águila vuela con una rapidez de 12m/s en la misma dirección a la que se mueve su presa. Un conejo que corre a rapidez de 5m/s. Si el águila está a 10 metros de distancia del conejo, ¿cuánto tiempo pasará para que lo alcance?

**Para el águila:**

La posición del águila está dada por:
$$
x_f=x_o+v_A\Delta t=x_o+v_A(\cancelto{t}{t_f}-\cancelto{0}{t_o})=x_o+v_A*t\\

x_f=x_A(t)=\cancelto{0}{x_o}+v_{A}t\\
x_A(t)=12t
$$

$$
x_A(0)=12(0)=0m\\
x_A(5)=12(5)=60m
$$
La posición del conejo está dada por:
$$
x_B(t)=10+5t
$$

$$
x_B(5)=10+5(5)=35
$$

Solucionando:
$$
x_A(t_R)=x_B(t_R)\\
12t_R=10+5t_R\\
(12-5)t_R=10\\
t_R=10/7\\
$$


**Ejemplo 2:** Encuentro

> Dos carros están separados una distancia de $100km$, y desean saber cuanto tiempo tardarán en juntarse. Si el carro $A$ se mueve a $80km/h$ y el carro $B$ a $100km/h$. ¿Cuánto será ese tiempo?

$$
x_A(t)=80t\\
x_B(t)=100-100t\\\\

x_A=x_B\\
80t=100-100t\\
t=100/180=5/9 h
$$







**Ejemplo 3:** Alcance con aceleración

> Mismo caso del águila, pero ahora el conejo se encuentra en reposo, y luego comienza a correr acelerando a $0.7m/s^2$. Determine si el águila alcanzará a su presa. Si lo hace, cuanto tiempo tarda.

$$
x_A=12t\\\\

d_B=x_f-x_o=v_ot+0.5a_Bt^2\\
x_f=x_B(t)=10+0.5(0.7)t^2\\
x_B(t)=10+0.35t^2\\

x_A(t_r)=x_B(t_r)\\
12t=10+0.35t^2\\
0.35t^2-12t+10=0\\
t_1=33.43, t_2=0.85
$$





## Técnica rápida

$d=x_f-x_o$

$\Delta t =t_f-\cancelto{0}{t_o}=t$



**$v,d,t$**
$$
v=\frac{d}{t}
$$
**$a,t,v_f,v_o$**
$$
a=\frac{v_f-v_o}{t}
$$
$d,v_o,a,t$
$$
d=v_0 t+\frac{1}{2}a t^2
$$
$d,v_f,a,t$
$$
d=v_f t-\frac{1}{2}a t ^2
$$
$v_o,v_f,a,d$
$$
2 ad=v_f^2-v_o^2\\
$$



###	5 datos distintos

$a, d, t, v_o, v_f$





##	Tiro parabólico

Separar el problema en partes, desde su altura máxima, y las dos alturas en las plataformas

Trabajar el problema separando $x$ y $y$.



**Ejemplo 1**

> Un libro de física que se desliza sobre una mesa horizontal a 1.10
> m>s cae al piso en 0.350 s. Ignore la resistencia del aire. Calcule a) la
> altura de la mesa; b) la distancia horizontal del borde de la mesa al
> punto donde cae el libro; c) las componentes horizontal y vertical, y la
> magnitud y dirección, de la velocidad del libro justo antes de tocar el
> piso. d) Dibuje gráficas x-t, y-t, v x -t y v y -t para el movimiento.

$h$, 



**X:**

$v_x=1.1m/s$

$d=?$

$t=0.35s$



**Y:**

$v_{oy}=0$

$t=0.35s$

$a=g=9.8m/s$





$h=v_{oy}t+\frac{1}{2}at^2=0.6m$

$v_{fy}=\sqrt{2gh}=3.43m/s$



$\mid v_f\mid=\sqrt{v_{fy}^2+v_x^2}=3.6m/s$

$\theta=arctan(v_{yf}/v_x)=72^\circ$





$tan(x)=opuesto/adyacente$

$x=tan^{-1}(opuesto/adyacente)$





###	Movimiento de proyectiles - casos

| x     | y     |
| ----- | ----- |
| $v_x$ | $a=g$ |
| t     | t     |
| x, d  | y, h  |
|       | $v_o$ |
|       | $v_f$ |
|       |       |

####	Caso 1 (más trivial)

En el eje $x$ nos den 2 datos, y falte 1

En el eje $y$ nos den 3 datos, y falten 2



#### Caso 2

En el eje $x$ nos den 2 datos (v, x), y no nos dan el tiempo

En el eje $y$ nos den 2 datos, falta el tiempo y otros dos datos

> Despejar el tiempo del eje $x$, y luego, utilizarlo en el eje $y$ como el tercer dato.



#### Caso 3

En el eje x nos dan 1 dato, y nos piden los otros 2

En el eje y nos dan 3 datos (sin incluir el tiempo), y nos piden los otros dos

> Despejar el tiempo del eje $y,$ y luego utilizarlo en el eje $x$ como el segundo dato





#####	Ejercicio 3.12

Tenemos el **Caso 3**

Encontramos el tiempo despejando de:
$$
t=\sqrt{2h/g}
$$
En el eje x, tenemos la distancia mínima y ahora también el tiempo

Despejamos la velocidad
$$
v=d/t=d\sqrt{g/2h}
$$
$v>1.29 m/s$





####	Ejercicio 3.14

Tenemos el **Caso 3**

Reciclamos la ecuación del inciso anterior
$$
v_1=d/t=d_1\sqrt{g/2h}=2.67m/s
$$

$$
v_2=d/t=d_2\sqrt{g/2h}=4.67m/s
$$

La velocidad puede ir de 2.67m/s a 4.67m/s

La velocidad se encuentra en $[2.67,4.67]$

Tenemos $2.67m/s<v<4.67m/s$ 



##	Movimiento circular

###	Movimiento circular uniforme



| Movimiento en línea recta | Movimiento circular |
| ------------------------- | ------------------- |
| $d$                       | $\theta$            |
| $t$                       | $t$                 |
| $v=d/t$                   | $\omega=\theta/t$   |
|                           |                     |

###	Movimiento circular uniformemente variado

| MRU                      | MCU                                       |
| ------------------------ | ----------------------------------------- |
| $d$                      | $\theta$                                  |
| $t$                      | $t$                                       |
| $v=d/t$                  | $\omega=\theta/t$                         |
| $a=v_f-v_0/t$            | $\alpha=\omega_f-\omega_o/t$              |
| $d=v_ot+\frac{1}{2}at^2$ | $\theta=\omega_ot+\frac{1}{2}\alpha t^2$  |
| $d=v_ft-\frac{1}{2}at^2$ | $\theta=\omega_f t-\frac{1}{2}\alpha t^2$ |
| $2ad=v_f^2-v_o^2$        | $2\alpha \theta=\omega_f^2-\omega_0^2$    |
|                          | $a_c=v^2/r$, $a_c=\omega^2r$              |
|                          |                                           |

**Período:** Es el tiempo que se tarda un objeto en dar una vuelta entera sobre su eje. $T=[s]$ 

**Frecuencia:** Cuántas vueltas da el objeto por **unidad** de tiempo en el sistema que se esté trabajando. $f=[rps=hertz],[rpm],[1/s],[1/min]$
$$
f=1/T
$$
$T=5m$

$f=1/5rps=2/10$





####	Ejemplos

3.28

> Imagine que, en su primer día de trabajo para un fabricante
> de electrodomésticos, le piden que averigüe qué hacerle al periodo de
> rotación de una lavadora para triplicar la aceleración centrípeta, y
> usted impresiona a su jefa contestando inmediatamente. ¿Qué le
> contesta?

$$
a_c=\omega^2 r\\
\omega=\theta/t=2\pi/T
$$

$$
a_c=4\pi^2 r/T^2
$$

$$
a_c=\frac{4\pi^2r}{((\beta T)^2}=3\frac{4\pi^r}{T^2}
$$

$$
\frac{4\pi^2r}{\beta^2 T^2}=3\frac{4\pi^2r}{T^2}\\
\frac{1}{\beta^2}=3\\
\beta=\sqrt{\frac{1}{3}}
$$






$$
a_c=3*4\pi^2 r/T^2\\
\frac{4\pi^2r}{\frac{1}{3}T^2}\\
\frac{4\pi^2r}{(\sqrt{\frac{1}{3}}T)^2}
$$




